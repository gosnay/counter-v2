var express = require('express');
var router = express.Router();
var moment = require('moment');
var request = require('request');
var _ = require('underscore');
var tick = {};
const uuid = require('uuidv4').default;

router.get('/', function (req, res, next) {
    if (req.query.id) {
        roomId = req.query.id
    } else {
        res.render('error', { errorMsg: "Missing Room ID" })
    };
    console.log(room[roomId])
    if (room[roomId]) {
        res.render('index', {
            timers: room[roomId]
        })
    } else {
        room[roomId] = {
            roomId,
            timer: []
        }
        res.render('index', {
            timers: room[roomId]
        })
    };
})

router.get('/start/:roomId/:timerId', function (req, res, next) {
    changeTimer(req.params.timerId, req.params.roomId);
    res.status(200).send('ok')
})

router.get('/create/:roomId/:timerName', function (req, res, next) {
    createTimer(req.params.timerName, req.params.roomId);
    res.status(200).send('ok')
})

router.post('/registerSocket', function (req, res, next) {
    let socketId = req.body.socketId;
    let roomId = req.body.roomId;
    sockets.push({
        room: socketId,
        roomId
    })
    res.status(200).send('ok');
})

changeTimer = (timerId, roomId) => {
    if (tick[roomId]) {
        clearInterval(tick[roomId]);
    }
    tick[roomId] = setInterval(function () {
        _.where(_.where(room, { roomId: roomId })[0].timer, { id: timerId })[0].value++
    }, 1000)
}

createTimer = (timerName, roomId) => {
    room[roomId].timer.push({
        id: uuid(),
        name: timerName,
        value: 0
    })
}

setInterval(() => {
    // console.log('---Sockets---');
    sockets.map((socket, key) => {
        if (io.sockets.connected[socket.room]) {
            io.sockets.connected[socket.room].emit('tick', room[socket.roomId])
        }
    })
    // Object.keys(io.sockets.sockets).map((socket) => {
    //     console.log(io.sockets.sockets[socket].connected);
    // })
}, 1000)

module.exports = router;