var socket = io();

socket.on('tick', function (data) {
    data.timer.map((counter) => {
        document.getElementById(`${counter.id}Counter`).innerHTML = formatSeconds(counter.value);
    })
})

socket.on('socketId', (data) => {
    var urlParams = new URLSearchParams(location.search);
    var roomId = urlParams.get('id');
    registerSocket(data, roomId);
    console.log(`Connected With Socket ID: ${data}`)
})

function start(timerId, roomId) {
    var filePath = `/start/${roomId}/${timerId}`;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", filePath, false);
    xmlhttp.send(null);
}

function createCounter(roomId) {
    let newCounter = document.getElementById("newCounterInput").value;
    var filePath = `/create/${roomId}/${newCounter}`;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", filePath, false);
    xmlhttp.send(null);
    location.reload();
}

function formatSeconds(secs) {
    function pad(n) {
        return (n < 10 ? "0" + n : n);
    }

    var h = Math.floor(secs / 3600);
    var m = Math.floor(secs / 60) - (h * 60);
    var s = Math.floor(secs - h * 3600 - m * 60);

    return pad(h) + ":" + pad(m) + ":" + pad(s);
}

function registerSocket(socketId, roomId){
    var filePath = `/registerSocket`;
    var paramstring;
    paramstring = `socketId=${socketId}&roomId=${roomId}`;
    var params = encodeURI(paramstring);
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", filePath, false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}