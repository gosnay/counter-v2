var express = require('express');
var app = express();
var server = require('http').createServer(app);
require('dotenv').config();
var session = require('express-session');
const bodyParser = require("body-parser");
var io = require('socket.io')(server);
global.io = io;
global.tick;
global.room = {};
global.sockets = []

app.use(session({
    secret: 'BlahBlahBlah',
    resave: false,
    saveUninitialized: true
}))

app.use(bodyParser.urlencoded({
    extended: true
}));

io.on('connection', function (socket) {
    io.sockets.connected[socket.id].emit('socketId', socket.id);
});

app.use(bodyParser.json());

app.set('port', 82);
app.set('view engine', 'ejs');
app.set('views', 'app/views');

app.use(express.static('app/public'));
app.use(require('./routes/index'));

server.listen(app.get('port'), function () {
    console.log('Listening on port ' + app.get('port'));
});